import React, { Component } from 'react';
import { Text, View, Alert, Image } from 'react-native';
import { Button, Input } from 'react-native-elements';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export default class SignInScreen2 extends Component {
  state = {
    username: '',
    password: '',
  };

  handleSignIn = async () => {
    const { username, password } = this.state;
    const { navigate } = this.props.navigation;
    // Alert.alert(username)
    try {
      
      const responseProfile = await axios.get('https://smartlife-exam.entercorp.net/user/profile', {
        auth: {
          username: username,
          password: password,
        },
      });
      const profile = responseProfile.data.data
      // const profile = responseProfile.data.data.attributes
      const jsonValue = JSON.stringify(profile)
      AsyncStorage.setItem('@profile', jsonValue)
      AsyncStorage.setItem('@username', username)
      AsyncStorage.setItem('@password', password)

      const ageUser = profile.attributes.age
      const responseAppVersion = await axios.post('https://smartlife-exam.entercorp.net/user/app/usage',{}, {
        auth: {
          username: username,
          password: password,
        },
        headers: { Prefer: (ageUser < 30 ? 'example=appUsageUser1Example' : 'example=appUsageUser2Example') },
      })
      const appVersion = responseAppVersion.data.data
      const jsonValueAppVersion = JSON.stringify(appVersion)
      AsyncStorage.setItem('@appVersion', jsonValueAppVersion)
      navigate('Main')
    } catch (error) {
      // Alert.alert(error.message)
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.headers);
        // res.status(error.response.status).send('something wrong')
        Alert.alert('something wrong');
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
        console.log(error)
        // res.status(500).send('something wrong')
        Alert.alert('network error');
        
      }
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    const { username, password } = this.state;
    return (
      <View style={{ width: '100%', height: '100%', backgroundColor: 'rgb(225,126,73)' }}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ alignItems: 'center', width: '80%', height: '100%' }}>
            <View style={{ alignItems: 'center', marginTop: '18%', marginBottom: 45 }}>
              <Image source={require('../image/MEA_logo.png')} />
              <Text style={{ color: '#fff', fontSize: 48, fontWeight: 'bold' }}>MEA</Text>
              <Text style={{ color: '#fff', fontSize: 34, fontWeight: 'bold' }}>Smart Life</Text>
            </View>
            <Input
              placeholder="username"
              name="username"
              placeholderTextColor="#fff"
              inputStyle={{ color: '#fff' }}
              containerStyle={{ color: '#fff', borderBottomColor: '#fff' }}
              onChangeText={(username) => this.setState({ username })}
            />
            <Input
              placeholder="password"
              placeholderTextColor="#fff"
              inputStyle={{ color: '#fff' }}
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry={true}
            />
            <Button
             
              onPress={this.handleSignIn}
              title="Sign In"
              titleStyle={{ fontSize: 20, fontWeight: 'bold', color: 'rgb(225,126,73)' }}
              buttonStyle={{
                backgroundColor: 'rgb(250,250,250)',
                borderRadius: 10,
                width: 200,
                marginTop: 15,
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}
