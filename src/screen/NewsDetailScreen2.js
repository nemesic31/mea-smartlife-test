import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Text, View, ScrollView, Image, TouchableOpacity, Linking } from 'react-native';
import HTMLView from 'react-native-htmlview';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class NewsDetailScreen2 extends Component {
  state = {
    news: {},
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const id = this.props.route.params.id;
    axios
      .get(`https://smartlife-exam.entercorp.net/news/${id}`, {
        headers: { Prefer: `example=exampleNewsId${id}` },
      })
      .then((response) => {
        const { data } = response.data;
        this.setState({ news: data.attributes });
      })
      .catch((error) => {
        this.setState({ error: 'Something just went wrong' });
      });
  };

  render() {
    const { navigate } = this.props.navigation;
    const { news } = this.state;

    if (news === null) {
      return (
        <View>
          <Text> no internet </Text>
        </View>
      );
    }
    return (
      <View style={{ backgroundColor: 'rgb(250,250,250)', flex: 1 }}>
        <ScrollView>
          <View>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={{ uri: news.image_url }}
                style={{
                  width: '100%',
                  height: 250,
                }}
              />
            </View>
            <View style={{ width: '90%', alignSelf: 'center', marginTop: 15 }}>
              <Text style={{ fontSize: 20, color: '#c6c6c6', fontWeight: 'bold' }}>
                {news.category}
              </Text>
              <Text style={{ fontSize: 26, fontWeight: 'bold', color: 'rgb(225,126,73)' }}>
                {news.title}
              </Text>
              <View style={{ marginTop: 10, marginBottom: 20 }}>
                <HTMLView value={news.content} />
              </View>
              <View style={{ alignItems: 'center', marginBottom: 50, marginTop: 10 }}>
                <TouchableOpacity
                  style={{ alignItems: 'center' }}
                  onPress={() => Linking.openURL(`${news.embed_link}`)}>
                  <Text style={{ fontSize: 20 }}>Read more</Text>
                  <Icon name="chevron-down" style={{ fontSize: 25 }} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
