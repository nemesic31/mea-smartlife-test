import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import QRCode from 'react-native-qrcode-svg';
import Barcode from 'react-native-barcode-builder';
import { ButtonGroup } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

export default class QRcodeMeterScreen extends Component {

  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      resultQrcode: {},
      resultBarcode: {},
      profile: null,
    };
  }

  componentDidMount = async () => {
    const profile = await AsyncStorage.getItem('@profile');
    this.setState({ profile: profile != null ? JSON.parse(profile) : null });
    this.getQRcode()
    this.getBarcode()

  };

  getQRcode = async () => {
    const { profile } = this.state;
    if (profile !== null) {

      axios
        .post('https://smartlife-exam.entercorp.net/payment/channels/barcode',
          {
            data: {
              type: "payment_barcodes",
              attributes: {
                type: "qrcode",
                data: `${profile.attributes.national_id}`,
                style: {
                  background: "#ffffff",
                  foreground: "#e17e49",
                  "display_value": false
                }
              }
            }
          }
        )
        .then((response) => {
          const { data } = response.data;
          this.setState({ resultQrcode: data });

        })
        .catch((error) => {
          this.setState({ error: 'Something just went wrong' });
        });
    }
  }

  getBarcode = async () => {
    const { profile } = this.state;
    if (profile !== null) {

      axios
        .post('https://smartlife-exam.entercorp.net/payment/channels/barcode',
          {
            data: {
              type: "payment_barcodes",
              attributes: {
                type: "barcode",
                data: `${profile.attributes.national_id}`,
                style: {
                  background: "#ffffff",
                  foreground: "#e17e49",
                  "display_value": true
                }
              }
            }
          }
        )
        .then((response) => {
          const { data } = response.data;
          this.setState({ resultBarcode: data });
        })
        .catch((error) => {
          this.setState({ error: 'Something just went wrong' });
        });
    }
  }

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };

  QRcode() {
    const { resultQrcode } = this.state;
    if (typeof resultQrcode.attributes !== 'undefined' && resultQrcode.attributes !== null) {
      return (
        <View>
          <Image
          style={{width:300,height:300}}
            source={{
              uri: `${resultQrcode.attributes.base64_image}`,
            }}
          />
        </View>
      );
    }
  }

  Barcode() {
    const { resultBarcode } = this.state;
    if (typeof resultBarcode.attributes !== 'undefined' && resultBarcode.attributes !== null) {
      return (
        <View>
          <Image
          style={{width:300,height:200}}
            source={{
              uri: `${resultBarcode.attributes.base64_image}`,
            }}
          />
        </View>
      );
    }
  }



  render() {
    const buttons = ['QRcode', 'Barcode']
    const { selectedIndex } = this.state;
    return (
      <View style={{
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(250,250,250)'
      }}>
        <View style={{ height: '40%', width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 15 }}>
          {this.state.selectedIndex === 0 ? this.QRcode() : this.Barcode()}
        </View>
        <View style={{ marginTop: 15, width: '100%' }}>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            textStyle={{ color: 'rgb(225,126,73)' }}
            containerStyle={{ height: 50 }}
            selectedButtonStyle={{ backgroundColor: 'rgb(225,126,73)' }}
          />
        </View>

      </View>
    )
  }
}
