import React, { Component } from 'react';
import { Text, View, TouchableHighlight, StyleSheet, Image } from 'react-native';
import { Card,Button } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';

export default class HomeScreen extends Component {
  state = {
    profile: null,
  };

  componentDidMount = async () => {
    const profile = await AsyncStorage.getItem('@profile');
    this.setState({ profile: profile != null ? JSON.parse(profile) : null });
  };


  render() {
    const { navigate } = this.props.navigation;
    const { profile } = this.state;
    const headerCard = `Hello ${profile && profile.attributes.customer_name}`

    return (
      <View style={{ flex: 1, backgroundColor: 'rgb(250,250,250)' }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          }}>
          <Card
            title= {headerCard}
            titleStyle = {{color:'rgb(225,126,73)'}}
            image={require('../image/MEA_logo.png')}
            imageStyle={{height:250}}
            containerStyle={{width:'70%',borderRadius:10,borderColor:'rgb(225,126,73)'}}
            >
            <Text style={styles.textDetail}>
              Name : {profile && profile.attributes.customer_name}
            </Text>
            <Text style={styles.textDetail}>Gender : {profile && profile.attributes.gender}</Text>
            <Text style={styles.textDetail}>Age : {profile && profile.attributes.age}</Text>
            <Button 
            title='pay' 
            buttonStyle={{backgroundColor:'rgb(225,126,73)',marginTop:15}} 
            titleStyle={{color:'#fff'}}
            type="clear"
            onPress={() => navigate('Payment')}
            />
          </Card>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(250,250,250)',
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'rgb(225,126,73)',
    paddingVertical: 50,
    borderRadius: 10,
    marginHorizontal: 8,
    marginTop: 8,
  },
  text: {
    color: 'rgb(250,250,250)',
    fontSize: 20,
    fontWeight: 'bold',
  },
  textDetail: {
    fontSize: 23,
    fontWeight: 'bold',
    color: 'rgb(225,126,73)',
  },
});
