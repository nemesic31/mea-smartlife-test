import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import SwiperFlatList from 'react-native-swiper-flatlist';
import AsyncStorage from '@react-native-community/async-storage';

export default class newsScreen extends Component {
  state = {
    news: [],
    isLoading: true,
    appVersion: null,
  };

  componentDidMount = async () => {
    this.setState({ news: [], isLoading: true });
    this.getData();
    const appVersion = await AsyncStorage.getItem('@appVersion');
    this.setState({ appVersion: appVersion != null ? JSON.parse(appVersion) : null });
  };

  getData = () => {
    axios
      .get('https://smartlife-exam.entercorp.net/news')
      .then((response) => {
        const { data } = response.data;
        this.setState({ isLoading: false, news: data });
        
      })
      .catch((error) => {
        this.setState({ isLoading: false, error: 'Something just went wrong' });
      });
  };

  onRefresh = () => {
    this.setState({ isLoading: true });
    this.getData();
  };

  renderRowA = () => {
    const showNewsCard = [];
    const { news } = this.state;
    const { navigate } = this.props.navigation;
    for (let i = 0; i < news.length; i++) {
      showNewsCard.push(
        <View key={i}>
          <TouchableOpacity onPress={() => navigate('newsDetail', { id: news[i].id })}>
            <View
              style={{
                backgroundColor: 'rgb(253,245,226)',
                borderRadius: 5,
                height: 150,
                marginTop: 5,
                marginBottom: 10,
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
              }}>
              <Image
                source={{
                  uri: news[i].attributes.image_url,
                }}
                style={{
                  width: '40%',
                  height: 150,
                  justifyContent: 'center',
                }}
              />
              <View style={{ justifyContent: 'center', width: '50%', marginRight: 15 }}>
                <Text
                  style={{
                    marginBottom: 5,
                    fontSize: 12,
                    color: 'rgb(225,126,73)',
                    fontWeight: 'bold',
                  }}>
                  {news[i].attributes.category}
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    color: 'rgb(225,126,73)',
                  }}>
                  {news[i].attributes.title}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
    return showNewsCard;
  };

  HighLightNewsCard = () => {
    const NewsCard = [];
    const { news } = this.state;
    const { navigate } = this.props.navigation;
    for (let i = 0; i < 3; i++) {
      NewsCard.push(
        <View key={i}>
          <TouchableOpacity onPress={() => navigate('newsDetail', { id: news[i].id })}>
            <View style={[styles.child, { backgroundColor: '#000' }]}>
              <Image
                source={{
                  uri: news[i].attributes.image_url,
                }}
                style={{
                  width: '100%',
                  height: '100%',
                  position: 'absolute',
                  opacity: 0.4,
                }}
              />
              <Text
                style={{
                  marginBottom: 5,
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'rgb(225,126,73)',
                }}>
                {news[i].attributes.title}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
    return NewsCard;
  };

  Swiper = () => {
    return (
      <View style={styles.container}>
        <SwiperFlatList autoplay autoplayDelay={2} autoplayLoop index={2} showPagination>
          {this.HighLightNewsCard()}
        </SwiperFlatList>
      </View>
    );
  };

  ScreenA = () => {
    const { news, isLoading } = this.state;
    return (
      <View style={{ backgroundColor: '#fff', height: '100%' }}>
        <Text style={{ fontSize: 35, margin: 15, fontWeight: 'bold', color: 'rgb(225,126,73)' }}>
          News Highlight
        </Text>
        {this.Swiper()}
        {isLoading ? (
          <ActivityIndicator size="large" color="rgb(225,126,73)" />
        ) : (
          <ScrollView
            ontentInsetAdjustmentBehavior="automatic"
            style={{ height: '100%' }}
            refreshControl={
              <RefreshControl
                refreshing={isLoading}
                onRefresh={this.onRefresh}
                tintColor="transparent"
                colors={['transparent']}
              />
            }>
            {this.renderRowA()}
          </ScrollView>
        )}
      </View>
    );
  };

  renderRowB = () => {
    const showNewsCard = [];
    const { news } = this.state;
    const { navigate } = this.props.navigation;
    for (let i = 0; i < news.length; i++) {
      showNewsCard.push(
        <View key={i}>
          <TouchableOpacity onPress={() => navigate('newsDetail', { id: news[i].id })}>
            <View style={{ width: '100%', alignItems: 'center' }}>
              <View
                style={{
                  backgroundColor: 'rgb(240,240,240)',
                  borderRadius: 5,
                  height: 450,
                  marginTop: 5,
                  marginBottom: 10,
                  width: '95%',
                  borderRadius: 5,
                }}>
                <Text
                  style={{
                    margin: 10,
                    fontSize: 20,
                    color: 'rgb(225,126,73)',
                    fontWeight: 'bold',
                  }}>
                  {news[i].attributes.title}
                </Text>
                <Image
                  source={{
                    uri: news[i].attributes.image_url,
                  }}
                  style={{
                    width: '100%',
                    height: 250,
                    justifyContent: 'center',
                    borderRadius: 5,
                  }}
                />

                <View style={{ justifyContent: 'center' }}>
                  <Text
                    style={{
                      marginBottom: 5,
                      fontSize: 16,
                      color: 'rgb(225,126,73)',
                      fontWeight: 'bold',
                    }}>
                    {news[i].attributes.category}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      color: 'rgb(225,126,73)',
                    }}>
                    {news[i].attributes.title}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
    return showNewsCard;
  };

  ScreenB = () => {
    const { news, isLoading } = this.state;
    return (
      <View style={{ backgroundColor: '#fff', height: '100%' }}>
        <Text style={{ fontSize: 35, margin: 15, fontWeight: 'bold', color: 'rgb(225,126,73)' }}>
          News
        </Text>
        {isLoading ? (
          <ActivityIndicator size="large" color="rgb(225,126,73)" />
        ) : (
          <ScrollView
            ontentInsetAdjustmentBehavior="automatic"
            style={{ height: '100%' }}
            refreshControl={
              <RefreshControl
                refreshing={isLoading}
                onRefresh={this.onRefresh}
                tintColor="transparent"
                colors={['transparent']}
              />
            }>
            {this.renderRowB()}
          </ScrollView>
        )}
      </View>
    );
  };

  handleShowScreen = () => {
    const { appVersion } = this.state;
    if (appVersion.attributes.app_version === 'A') {
      return <View>{this.ScreenA()}</View>;
    } else {
      return <View>{this.ScreenB()}</View>;
    }
  };

  render() {
    const { news, appVersion } = this.state;
    if (!news || news.length === 0) {
      return (
        <View>
          <Text> no internet </Text>
        </View>
      );
    } 
    else if (!appVersion || appVersion === null) {
      return (
        <View>
          <Text> no internet </Text>
        </View>
      );
    }

    return <View>{this.handleShowScreen()}</View>;
  }
}
export const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    height: '35%',
    backgroundColor: 'white',
  },
  child: {
    height: height * 0.25,
    width,
    justifyContent: 'flex-end',
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center',
  },
});
