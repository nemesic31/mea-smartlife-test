import React, { Component } from 'react'
import { Text, View,TouchableOpacity } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';


export default class SettingScreen extends Component {
  state = {
    profile: null,
  };

  componentDidMount = async () => {
    const profile = await AsyncStorage.getItem('@profile');
    this.setState({ profile: profile != null ? JSON.parse(profile) : null });
  };
  render() {
    const { navigate } = this.props.navigation;
    const { profile } = this.state;
    return (
      <View >
        <View style={{ backgroundColor: '#fff',height:'100%'}}>

        <ListItem
          containerStyle={{marginTop:10}}
            title={profile && profile.attributes.customer_name}
            subtitle= {profile && profile.attributes.age}
            titleStyle={{ fontSize: 20, color: 'rgb(225,126,73)' }}
            subtitleStyle={{ fontSize: 20, color: 'rgb(225,126,73)', marginTop:5 }}
            leftIcon={{
              name: 'user',
              type: 'font-awesome',
              color: 'rgb(225,126,73)',
              size: 90,
              marginRight: 10,
              marginLeft: 10,
            }}
          />
        <Divider style={{ backgroundColor: 'rgb(225,126,73)',marginTop:10 }} />
        <TouchableOpacity  onPress={() => navigate('signIn')}>
        <ListItem
            title="SignOut"
            
            titleStyle={{ fontSize: 20, color: 'rgb(225,126,73)' }}
            rightIcon={{ name: 'navigate-next', color: 'rgb(225,126,73)' }}
            leftIcon={{
              name: 'power-off',
              type: 'font-awesome',
              color: 'rgb(225,126,73)',
              size: 25,
              marginRight: 10,
              marginLeft: 10,
            }}
            />
            </TouchableOpacity>
          <Divider style={{ backgroundColor: 'rgb(225,126,73)' }} />
        </View>

      </View>
    )
  }
}
