import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Entypo';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import SwiperFlatList from 'react-native-swiper-flatlist';

const styles = StyleSheet.create({
  colorBackground: { backgroundColor: 'rgb(250,250,250)', height: '100%' },
  textColor: { fontSize: 20, color: 'rgb(225,126,73)' },
});

export default class FavoriteScreen extends Component {
  state = {
    paymentChanelsFavorite: [],
    paymentChanelsFavoriteDebug: [],
    paymentChanelsRecommend: [],
    addFavorite: {},
    paymentChanels: [],
    normalStatus: '#fff',
    favorites: [],
    selectedIndex: 0,
    viewPaymentRecommendStatus: 0,
    viewPaymentStatus: 0,
  };

  componentDidMount() {
    this.getData();
    this.getDataPaymentFavorite();
    this.getDataPaymentRecommend();
  }

  onRefresh = () => {
    this.getData();
    this.getDataPaymentFavorite();
    this.getDataPaymentRecommend();
  };

  getData = () => {
    axios
      .get('https://smartlife-exam.entercorp.net/payment/channels')
      .then((response) => {
        const { data } = response.data;
        this.setState({ paymentChanels: data });
      })
      .catch((error) => {
        this.setState({ error: 'Something just went wrong' });
      });
  };

  getDataPaymentRecommend = async () => {
    const username = await AsyncStorage.getItem('@username');
    const password = await AsyncStorage.getItem('@password');
    axios
      .get('https://smartlife-exam.entercorp.net/payment/channels/recommend', {
        auth: {
          username: username,
          password: password,
        },
      })
      .then((response) => {
        const data = response.data.data;
        this.setState({ paymentChanelsRecommend: data });
      })
      .catch((error) => {
        this.setState({ error: 'Something just went wrong' });
      });
  };

  getDataPaymentFavorite = async () => {
    const username = await AsyncStorage.getItem('@username');
    const password = await AsyncStorage.getItem('@password');
    axios
      .get('https://smartlife-exam.entercorp.net/payment/favorites', {
        auth: {
          username: username,
          password: password,
        },
      })
      .then((response) => {
        const { data, included } = response.data;
        this.setState({ paymentChanelsFavoriteDebug: data });
        this.setState({ paymentChanelsFavorite: included });
        console.log(this.state.paymentChanelsFavorite.length);
      })
      .catch((error) => {
        this.setState({ error: 'Something just went wrong' });
      });
  };

  cardNull() {
    return (
      <View
        style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20, marginBottom: 20 }}>
        <Text style={{ color: 'rgb(225,126,73)', fontSize: 20 }}> null </Text>
      </View>
    );
  }

  handleClickFavorite = async (i) => {
    this.getDataPaymentFavorite();
    const username = await AsyncStorage.getItem('@username');
    const password = await AsyncStorage.getItem('@password');
    const { paymentChanelsFavorite, paymentChanels, paymentChanelsFavoriteDebug } = this.state;
    const paymentID = paymentChanels[i].id;
    if (paymentChanelsFavoriteDebug.length !== 0) {
      for (let j = 0; j < paymentChanelsFavorite.length; j++) {
        if (paymentID !== paymentChanelsFavorite[j].id) {
          axios
            .post(
              'https://smartlife-exam.entercorp.net/payment/favorites',
              {
                data: {
                  type: 'favorite_payment_channels',
                  attributes: {
                    payment_channel_id: paymentID,
                  },
                },
              },
              {
                auth: {
                  username: username,
                  password: password,
                },
              }
            )
            .then((response) => {
              const { data } = response.data;
              this.setState({ addFavorite: data });
              this.onRefresh();
            })
            .catch((error) => {
              this.setState({ error: 'Something just went wrong' });
              console.log(error);
            });
        }
      }
    } else {
      axios
        .post(
          'https://smartlife-exam.entercorp.net/payment/favorites',
          {
            data: {
              type: 'favorite_payment_channels',
              attributes: {
                payment_channel_id: paymentID,
              },
            },
          },
          {
            auth: {
              username: username,
              password: password,
            },
          }
        )
        .then((response) => {
          const { data } = response.data;
          this.setState({ addFavorite: data });
          this.onRefresh();
        })
        .catch((error) => {
          this.setState({ error: 'Something just went wrong' });
          console.log(error);
        });
    }
  };

  handleClickRemoveFavorite = async (i) => {
    const username = await AsyncStorage.getItem('@username');
    const password = await AsyncStorage.getItem('@password');
    const { paymentChanelsFavorite } = this.state;
    const id = paymentChanelsFavorite[i].attributes.favorite_payment_id;

    axios
      .delete(`https://smartlife-exam.entercorp.net/payment/favorites/${id}`, {
        auth: {
          username: username,
          password: password,
        },
      })
      .then((response) => {
        const { data } = response.data;
        this.onRefresh();
      })
      .catch((error) => {
        this.setState({ error: 'Something just went wrong' });
        console.log(error);
      });
  };

  showPaymentChannel = () => {
    const showPaymentCards = [];
    const { paymentChanels } = this.state;
    for (let i = 0; i < paymentChanels.length; i++) {
      showPaymentCards.push(
        <View key={i}>
          <Card containerStyle={{ backgroundColor: 'rgb(225,126,73)', borderRadius: 5 }}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanels.length === 0 ? '' : paymentChanels[i].attributes.channel}
              </Text>
              <TouchableOpacity onPress={() => this.handleClickFavorite(i)}>
                <Icon name="heart" size={20} style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanels.length === 0 ? '' : paymentChanels[i].attributes.payment_method}
              </Text>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                fee{' '}
                {paymentChanels.length === 0
                  ? ''
                  : paymentChanels[i].attributes.transaction_fee_value}
              </Text>
            </View>
          </Card>
        </View>
      );
    }

    return showPaymentCards;
  };

  showPaymentChannelsFavorite = () => {
    const showPaymentCardsFavorite = [];
    const { paymentChanelsFavorite } = this.state;
    if (typeof paymentChanelsFavorite !== 'undefined' && paymentChanelsFavorite !== null) {
      for (let i = 0; i < paymentChanelsFavorite.length; i++) {
        showPaymentCardsFavorite.push(
          <View key={i}>
            <Card containerStyle={{ backgroundColor: 'rgb(225,126,73)', borderRadius: 5 }}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                  marginTop: 5,
                }}>
                <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                  {paymentChanelsFavorite[i].attributes.channel}
                </Text>
                <TouchableOpacity onPress={() => this.handleClickRemoveFavorite(i)}>
                  <Icon name="heart" size={20} style={{ color: '#D11E49' }} />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'space-between',
                  marginTop: 5,
                }}>
                <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                  {paymentChanelsFavorite.length !== 0 &&
                    paymentChanelsFavorite[i].attributes.payment_method}
                </Text>
                <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                  fee{' '}
                  {paymentChanelsFavorite.length !== 0 &&
                    paymentChanelsFavorite[i].attributes.transaction_fee_value}
                </Text>
              </View>
            </Card>
          </View>
        );
      }
      return showPaymentCardsFavorite;
    }
  };

  showPaymentChannelsRecommend = () => {
    const showPaymentCardsRecommend = [];
    const { paymentChanelsRecommend } = this.state;
    for (let i = 0; i < paymentChanelsRecommend.length; i++) {
      showPaymentCardsRecommend.push(
        <View key={i}>
          <Card containerStyle={{ backgroundColor: 'rgb(225,126,73)', borderRadius: 5 }}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.channel}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.payment_method}
              </Text>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                fee{' '}
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.transaction_fee_value}
              </Text>
            </View>
          </Card>
        </View>
      );
    }
    return showPaymentCardsRecommend;
  };

  handleShowCardsPaymentFavorite = () => {
    const { paymentChanelsFavorite } = this.state;
    if (typeof paymentChanelsFavorite !== 'undefined' && paymentChanelsFavorite !== null) {
      return <View>{this.showPaymentChannelsFavorite()}</View>;
    } else {
      return <View>{this.cardNull()}</View>;
    }
  };

  handleViewAllPaymentRecommend = (viewPaymentRecommendStatus) => {
    if (viewPaymentRecommendStatus === 0) {
      this.setState({ viewPaymentRecommendStatus: 1 });
    } else {
      this.setState({ viewPaymentRecommendStatus: 0 });
    }
  };

  handleViewAllPayment = (viewPaymentStatus) => {
    if (viewPaymentStatus === 0) {
      this.setState({ viewPaymentStatus: 1 });
    } else {
      this.setState({ viewPaymentStatus: 0 });
    }
  };

  cardSwiperPaymentRecommend = () => {
    const { paymentChanelsRecommend } = this.state;
    const paymentSwiperCardChanelsRecommend = [];
    if (paymentChanelsRecommend.length !== 0) {
      for (let i = 0; i < paymentChanelsRecommend.length; i++) {
        paymentSwiperCardChanelsRecommend.push(
          <View key={i} style={{ marginLeft: -12 }}>
            <Card
              containerStyle={{
                backgroundColor: 'rgb(225,126,73)',
                borderRadius: 5,
                justifyContent: 'center',
                width: 300,
                height: 110,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.channel}
              </Text>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.payment_method}
              </Text>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                fee{' '}
                {paymentChanelsRecommend.length !== 0 &&
                  paymentChanelsRecommend[i].attributes.transaction_fee_value}
              </Text>
            </Card>
          </View>
        );
      }
      return paymentSwiperCardChanelsRecommend;
    }
  };

  cardSwiperPayment = () => {
    const { paymentChanels } = this.state;
    const paymentSwiperCardChanels = [];
    for (let i = 0; i < 6; i++) {
      paymentSwiperCardChanels.push(
        <View key={i} style={{ marginLeft: -12 }}>
          <Card
            containerStyle={{
              backgroundColor: 'rgb(225,126,73)',
              borderRadius: 5,
              justifyContent: 'center',
              width: 300,
              height: 110,
            }}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 5,
              }}>
              <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
                {paymentChanels.length === 0 ? '' : paymentChanels[i].attributes.channel}
              </Text>
              <TouchableOpacity onPress={() => this.handleClickFavorite(i)}>
                <Icon name="heart" size={20} style={{ color: '#fff' }} />
              </TouchableOpacity>
            </View>
            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
              {paymentChanels.length !== 0 && paymentChanels[i].attributes.payment_method}
            </Text>
            <Text style={{ fontWeight: 'bold', fontSize: 17, color: '#fff' }}>
              fee{' '}
              {paymentChanels.length !== 0 && paymentChanels[i].attributes.transaction_fee_value}
            </Text>
          </Card>
        </View>
      );
    }
    return paymentSwiperCardChanels;
  };

  swiperPaymentRecommend = () => {
    return (
      <View style={{ width: '100%', marginBottom: 10 }}>
        <SwiperFlatList index={0}>{this.cardSwiperPaymentRecommend()}</SwiperFlatList>
      </View>
    );
  };

  swiperPayment = () => {
    return (
      <View style={{ width: '100%' }}>
        <SwiperFlatList index={0}>{this.cardSwiperPayment()}</SwiperFlatList>
      </View>
    );
  };

  viewAll = () => {
    return <Text>View All</Text>;
  };
  viewLess = () => {
    return <Text>View Less</Text>;
  };

  handleSwiperAndListAllPaymentRecommend = () => {
    const { viewPaymentRecommendStatus, paymentChanelsRecommend } = this.state;

    if (paymentChanelsRecommend.length === 0) {
      return <View>{this.cardNull()}</View>;
    } else {
      return (
        <View style={{ margin: -3 }}>
          {viewPaymentRecommendStatus === 0
            ? this.swiperPaymentRecommend()
            : this.showPaymentChannelsRecommend()}
        </View>
      );
    }
  };

  handleSwiperAndListAllPayment = () => {
    const { viewPaymentStatus, paymentChanels } = this.state;

    if (paymentChanels.length === 0) {
      return <View>{this.cardNull()}</View>;
    } else {
      return (
        <View>{viewPaymentStatus === 0 ? this.swiperPayment() : this.showPaymentChannel()}</View>
      );
    }
  };

  render() {
    const { viewPaymentRecommendStatus, viewPaymentStatus, paymentChanelsRecommend } = this.state;
    if (paymentChanelsRecommend.length === 0) {
      return (
        <View>
          <Text> no internet </Text>
        </View>
      );
    }
    return (
      <View style={styles.colorBackground}>
        <ScrollView>
          <View style={{ margin: 15 }}>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
              }}>
              <Text style={styles.textColor}>Payment Favorite</Text>
            </View>
            <View>{this.handleShowCardsPaymentFavorite()}</View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 15,
              }}>
              <Text style={styles.textColor}>Payment Recommend</Text>
              <TouchableOpacity
                style={{ alignSelf: 'flex-end' }}
                onPress={() => this.handleViewAllPaymentRecommend(viewPaymentRecommendStatus)}>
                <View>{viewPaymentRecommendStatus === 0 ? this.viewAll() : this.viewLess()}</View>
              </TouchableOpacity>
            </View>
            {this.handleSwiperAndListAllPaymentRecommend()}
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'space-between',
                marginTop: 15,
              }}>
              <Text style={styles.textColor}>Payment All</Text>
              <TouchableOpacity
                style={{ alignSelf: 'flex-end' }}
                onPress={() => this.handleViewAllPayment(viewPaymentStatus)}>
                <View>{viewPaymentStatus === 0 ? this.viewAll() : this.viewLess()}</View>
              </TouchableOpacity>
            </View>
            {this.handleSwiperAndListAllPayment()}
          </View>
        </ScrollView>
      </View>
    );
  }
}
