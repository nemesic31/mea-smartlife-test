import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../screen/HomeScreen';
import SettingScreen from '../screen/SettingScreen';
import QRcodeMeterScreen from '../screen/QRcodeMeterScreen';
import FavoriteScreen from '../screen/FavoriteScreen';
import SignInScreen2 from '../screen/SignInScreen2';
import NewsScreen2 from '../screen/NewsScreen2';
import NewsDetailScreen2 from '../screen/NewsDetailScreen2';

import { View, Text, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';

import Icon from 'react-native-vector-icons/Ionicons';
import IconQR from 'react-native-vector-icons/FontAwesome';
import IconBar from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createBottomTabNavigator();

function HomeTab() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerLeft: false,
          headerTitle: 'Smartlife',
          headerTitleStyle: {
            color: '#fff',
          },
          headerStyle: {
            backgroundColor: 'rgb(225,126,73)',
          },
        }}
      />
    </Stack.Navigator>
  );
}


function NewsTab() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="News"
        component={NewsScreen2}
        options={{
          headerLeft: false,
          headerTitle: 'News',
          headerTitleStyle: {
            color: '#fff',
          },
          headerStyle: {
            backgroundColor: 'rgb(225,126,73)',
          },
        }}
      />
    </Stack.Navigator>
  );
}

function SettingTab() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Setting"
        component={SettingScreen}
        options={{
          headerLeft: false,
          headerTitle: 'Setting',
          headerTitleStyle: {
            color: '#fff',
          },
          headerStyle: {
            backgroundColor: 'rgb(225,126,73)',
          },
        }}
      />
    </Stack.Navigator>
  );
}

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: 'rgb(225,126,73)',
      }}>
      <Tab.Screen
        name="Home"
        component={HomeTab}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => <IconBar name="home" color={color} size={size} />,
          headerTitle: 'Home',
        }}
      />
      <Tab.Screen
        name="News"
        component={NewsTab}
        options={{
          tabBarLabel: 'News',
          tabBarIcon: ({ color, size }) => (
            <IconBar name="newspaper-variant" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Setting"
        component={SettingTab}
        options={{
          tabBarLabel: 'Setting',
          tabBarIcon: ({ color, size }) => (
            <IconBar name="cog" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

export default function AppNavigator() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="signIn" component={SignInScreen2} options={{ headerShown: false }} />
        <Stack.Screen name="Main" component={MyTabs} options={{ headerShown: false }} />
        <Stack.Screen
        name="Payment"
        component={FavoriteScreen}
        options={({ navigation }) => ({
          headerTitle: 'Payment',
          headerTitleStyle: {
            color: '#fff',
          },
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ flexDirection: 'row' }}>
              <Icon name="chevron-back" style={{ fontSize: 22, color: '#fff' }} />
              <Text style={{ fontSize: 20, fontWeight: '500', color: '#fff' }}>Back</Text>
            </TouchableOpacity>
          ),

          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.navigate('QRcodeMeter')}
              style={{ flexDirection: 'row' }}>
              <Text style={{ fontSize: 20, fontWeight: '500', color: '#fff', marginRight: 5 }}>
                QRcode
              </Text>
              <IconQR name="qrcode" style={{ fontSize: 22, color: '#fff', marginRight: 10 }} />
            </TouchableOpacity>
          ),
          headerStyle: {
            backgroundColor: 'rgb(225,126,73)',
          },
        })}
      />
        <Stack.Screen
          name="QRcodeMeter"
          component={QRcodeMeterScreen}
          options={({ navigation }) => ({
            headerTitle: 'QRcodeMeter',
            headerTitleStyle: {
              color: '#fff',
            },
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{ flexDirection: 'row' }}>
                <Icon name="chevron-back" style={{ fontSize: 22, color: '#fff' }} />
                <Text style={{ fontSize: 20, fontWeight: '500', color: '#fff' }}>Back</Text>
              </TouchableOpacity>
            ),
            headerStyle: {
              backgroundColor: 'rgb(225,126,73)',
            },
          })}
        />
        <Stack.Screen
          name="newsDetail"
          component={NewsDetailScreen2}
          options={({ navigation }) => ({
            headerTitle: 'News Detail',
            headerTitleStyle: {
              color: '#fff',
            },
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{ flexDirection: 'row' }}>
                <Icon name="chevron-back" style={{ fontSize: 22, color: '#fff' }} />
                <Text style={{ fontSize: 20, fontWeight: '500', color: '#fff' }}>Back</Text>
              </TouchableOpacity>
            ),
            headerStyle: {
              backgroundColor: 'rgb(225,126,73)',
            },
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
